package ru.pisarev.tm.repository;

import ru.pisarev.tm.api.IProjectRepository;
import ru.pisarev.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> list = new ArrayList<>();

    @Override
    public List<Project> findAll(){
        return list;
    }

    @Override
    public void add(Project project){
        list.add(project);
    }

    @Override
    public void remove(Project project){
        list.remove(project);
    }

    @Override
    public void clear(){
        list.clear();
    }

}
