package ru.pisarev.tm.service;

import ru.pisarev.tm.api.ITaskRepository;
import ru.pisarev.tm.api.ITaskService;
import ru.pisarev.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void add(final Task task) {
        if (task==null) return;
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task==null) return;
        taskRepository.remove(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }
}
