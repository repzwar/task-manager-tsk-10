package ru.pisarev.tm.service;

import ru.pisarev.tm.api.IProjectRepository;
import ru.pisarev.tm.api.IProjectService;
import ru.pisarev.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void add(final Project project) {
        if (project==null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project==null) return;
        projectRepository.remove(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

}
