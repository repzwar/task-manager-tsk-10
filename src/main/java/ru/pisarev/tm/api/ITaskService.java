package ru.pisarev.tm.api;

import ru.pisarev.tm.model.Task;

import java.util.List;

public interface ITaskService {
    List<Task> findAll();

    void add(Task task);

    void remove(Task task);

    void clear();
}
