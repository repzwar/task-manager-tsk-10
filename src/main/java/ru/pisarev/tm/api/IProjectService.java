package ru.pisarev.tm.api;

import ru.pisarev.tm.model.Project;

import java.util.List;

public interface IProjectService {
    List<Project> findAll();

    void add(Project project);

    void remove(Project project);

    void clear();
}
