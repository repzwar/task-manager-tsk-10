package ru.pisarev.tm.api;

import ru.pisarev.tm.model.Project;

public interface IProjectController {
    void showList();

    void create();

    Project add(String name, String description);

    void clear();
}
