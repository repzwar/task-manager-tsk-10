package ru.pisarev.tm.controller;

import ru.pisarev.tm.api.ITaskController;
import ru.pisarev.tm.api.ITaskService;
import ru.pisarev.tm.model.Task;
import ru.pisarev.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showList(){
        final List<Task> tasks= taskService.findAll();
        int index=1;
        for (Task task:tasks){
            System.out.println(index + ". " + task.toString());
            index++;
        }
    }

    @Override
    public void create(){
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Task task = add( name, description);
        if (task==null){
            System.out.println("Incorrect  values");
            return;
        }
        taskService.add(task);
    }

    @Override
    public Task add(final String name, final String description){
        if (name==null || name.isEmpty()) return null;
        if (description==null || description.isEmpty()) return null;
        return new Task(name, description);
    }

    @Override
    public void clear(){
        taskService.clear();
    }

}
