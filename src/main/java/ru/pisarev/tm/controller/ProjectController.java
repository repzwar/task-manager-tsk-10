package ru.pisarev.tm.controller;

import ru.pisarev.tm.api.IProjectService;
import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements ru.pisarev.tm.api.IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showList(){
        final List<Project> projects= projectService.findAll();
        int index=1;
        for (Project project:projects){
            System.out.println(index + ". " + project.toString());
            index++;
        }
    }

    @Override
    public void create(){
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Project project = add( name, description);
        if (project==null){
            System.out.println("Incorrect  values");
            return;
        }
        projectService.add(project);
    }

    @Override
    public Project add(final String name, final String description){
        if (name==null || name.isEmpty()) return null;
        if (description==null || description.isEmpty()) return null;
        return new Project(name, description);
    }

    @Override
    public void clear(){
        projectService.clear();
    }

}
