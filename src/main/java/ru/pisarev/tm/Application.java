package ru.pisarev.tm;

import ru.pisarev.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(final String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
